# Louvain-li-Nux Site
Website of the Louvain-li-Nux KAP.

Bellow three ways: gitlab-ci, docker and standalone, are described to deploy, test and edit the website.

**The gitlab-ci or docker ways are recommended.**

# Gitlab-CI
The pipeline job will create a new docker image with the content of the gitlab repository.
The newly created image will then be pushed to our gitlab image registry.

## How to?
1. Push commit to the master branch of the repository
2. Run the gitlab ci pipeline job.
3. If the job succeeded, the *louvainlinux/site* image will be updated in the gitlab louvainlinux container registry.
4. The image can be pulled at wish

# Docker
### Run with docker (Cfr. dockerized-llnux-services for deployment in production)
0. Install docker
1. Build the docker image
```sh
docker build -t image_name .
```
2. Start the docker container with the previously builded image
```sh
docker run -p 3000:3000 --name container_name image_name
```
3. Acces the website via
<http://127.0.0.1:3000>

### Local testing
If you have a docker container already started and you want to keep the same name, you need to *stop it* and *delete it* with theses commands:
```sh
docker container stop container_name
docker container rm container_name
```
Each modification of the site needs a rebuild of the image with the command:
```sh
docker build -t image_name .
```
Then rerun the container with the command given above.

### Edit website in production
See readme at <https://gitlab.com/louvainlinux/dockerized-llnux-services/-/tree/master>


# Standalone Deployment
### Run
1. Install `npm`
2. Install gulp:
```sh
# npm install -g gulp
```
3. clone this repo and move into it:
4. install dependencies:
```sh
npm install --dev
```
5. build:
```sh
gulp deploy
```

### Local testing
Make the deployment steps, and then (in the `site` directory) run `gulp serve`.

### Workflow to edit the website
1. Make a local clone
2. Edit the required file
3. Test on local machine
4. Commit and push to github
5. On the VPS, `cd /var/www/louvainlinux`, `git pull` and restart the service:
```sh
# systemctl restart site_llnux.service
```

# What to edit ?

Edit mainly the templates in `views/pages/`. The templating language doc is
here: <https://pugjs.org/api/getting-started.html>. You can create new files,
but no sub-directory.

### Edit the dates of the activities
You just need to edit the files in `views/mixins/dates/`.

One file corresponds to an activity and you have to modify only 3 variables:

1. **start_date**	=>		The start date of the activity (Date Object)
2. **end_date**		=>		The end date of the activity   (Date Object)
3. **location**		=>		The location of the activity

with `Date(YEAR(yyyy), MONTH(0-11), DAY(1-31) HOURS(0-23), MINS(0-59))`
