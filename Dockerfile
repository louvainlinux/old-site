FROM node:10  

WORKDIR /app

COPY ["package.json", "package-lock.json*","gulpfile.js",".","./"]

RUN npm install -g gulp
RUN npm install 
RUN gulp deploy --production

ENV NODE_ENV=production

CMD ["gulp","serve"]
